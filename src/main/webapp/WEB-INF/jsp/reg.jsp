<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" type="text/css"
	href=${pageContext.request.contextPath}/css/all.css />
</head>
<body>
	<h1>Rgistration FORM</h1>
	<sf:form commandName="spitter" method="POST" enctype="multipart/form-data">
	Name:<sf:input path="name" />
		<sf:errors path="name" cssClass="error"></sf:errors>
		<br>
	Surname:<sf:input path="surname" />
		<sf:errors path="surname" cssClass="error"></sf:errors>
		<br>
	Password:<sf:password path="password" />
		<sf:errors path="password" cssClass="error"></sf:errors>
		<br>
	Email:<sf:input path="email" type="email" />
		<sf:errors path="email" cssClass="error"></sf:errors>
		<br>
		<input type="file" name="profilePicture" accept="image/jpeg,image/png,image/gif">
		<br>
		<input type="submit" value="Register" />
	</sf:form>
</body>
</html>