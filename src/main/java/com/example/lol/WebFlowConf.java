package com.example.lol;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.webflow.config.AbstractFlowConfiguration;
import org.springframework.webflow.definition.registry.FlowDefinitionRegistry;
import org.springframework.webflow.engine.builder.support.FlowBuilderServices;
import org.springframework.webflow.executor.FlowExecutor;
import org.springframework.webflow.mvc.builder.MvcViewFactoryCreator;

import java.util.Arrays;

@Configuration
public class WebFlowConf extends AbstractFlowConfiguration {

    @Bean
    public FlowDefinitionRegistry flowRegistry(FlowBuilderServices service) {
        return getFlowDefinitionRegistryBuilder().setBasePath("/WEB-INF/flows/").addFlowLocationPattern("**/*-flow.xml")
                .setFlowBuilderServices(service).build();
    }

    @Bean
    public FlowExecutor flowExecutor(FlowDefinitionRegistry reg) {
        return getFlowExecutorBuilder(reg).build();
    }

    @Bean
    public FlowBuilderServices builderServices(MvcViewFactoryCreator viewFactoryCreator) {
        return getFlowBuilderServicesBuilder().setViewFactoryCreator(viewFactoryCreator).build();
    }

    @Bean
    public MvcViewFactoryCreator mvcViewFactoryCreator(ViewResolver viewResolver) {
        MvcViewFactoryCreator mvcViewFactoryCreator = new MvcViewFactoryCreator();
        mvcViewFactoryCreator.setViewResolvers(Arrays.asList(viewResolver));
        return mvcViewFactoryCreator;
    }

}
