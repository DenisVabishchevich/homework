package com.example.lol.coredi;

import org.springframework.stereotype.Component;

@Component
public class MyCD implements CD {

    private String artist = "Звери";
    private String song = "Капканы";

    public void play() {
	System.out.println(" Playing :" + song + " by " + artist);
    }

}
