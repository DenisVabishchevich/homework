package com.example.lol.lol;

import java.beans.PropertyEditorSupport;

public class AdressEditor extends PropertyEditorSupport {

    @Override
    public void setAsText(String text) throws IllegalArgumentException {

	Adress a = new Adress();
	a.setRandom(Integer.parseInt(text));

	setValue(a);
    }

}
