package com.example.lol.lol;

import org.springframework.stereotype.Component;

@Component
public class Adress {
    private int random;

    public Adress() {
	// TODO Auto-generated constructor stub
    }

    public Adress(int random) {
	super();
	this.random = random;
    }

    public int getRandom() {
	return random;
    }

    public void setRandom(int random) {
	this.random = random;
    }

    @Override
    public String toString() {
	return "Adress [random=" + random + "]";
    }

}
