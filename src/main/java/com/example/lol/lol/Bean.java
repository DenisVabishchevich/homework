package com.example.lol.lol;

import org.springframework.beans.factory.annotation.Configurable;

@Configurable("bean")
public class Bean {

    private Adress adress;
    private String name;
    private String disc;

    public Bean() {
    }

    public String getName() {
	return name;
    }

    public void setName(String name) {
	this.name = name;
    }

    public String getDisc() {
	return disc;
    }

    public void setDisc(String disc) {
	this.disc = disc;
    }

    public Adress getAdress() {
	return adress;
    }

    public void setAdress(Adress adresses) {
	this.adress = adresses;
    }

    @Override
    public String toString() {
	return "Bean [adress=" + adress + ", name=" + name + ", disc=" + disc + "]";
    }

}
