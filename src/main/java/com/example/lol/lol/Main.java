package com.example.lol.lol;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Main {

    public static void main(String[] args) {

	ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("beans.xml");

	Bean bean = context.getBean(Bean.class);
	System.out.println(bean.getAdress());

	context.close();

    }
}
