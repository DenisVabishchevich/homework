package com.example.lol.lol;

public class IntHolder {
    private int random = (int) (Math.random() * 100);

    public int getRandom() {
	return random;
    }

    public void setRandom(int random) {
	this.random = random;
    }

    @Override
    public String toString() {
	return "IntHolder [random=" + random + "]";
    }

}
