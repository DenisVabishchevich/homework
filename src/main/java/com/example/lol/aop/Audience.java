package com.example.lol.aop;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class Audience {

    @Pointcut("execution(* com.example.lol.aop.Performance.perform(..))")
    public void perf() {
    }

    @Before("perf()")
    public void cellOff() {
	System.err.println("Turn off cellphones");
    }

    @After("perf()")
    public void afterAllFinished() {
	System.err.println("Every one go home");
    }

    @Around("perf()")
    public void around(ProceedingJoinPoint jp) throws Throwable {

	System.err.println("some stuff before");
	jp.proceed();
	System.err.println("some stuff after");

    }

}
