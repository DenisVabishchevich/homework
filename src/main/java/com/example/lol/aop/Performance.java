package com.example.lol.aop;

public interface Performance {

    void perform();

}
