package com.example.lol;

import com.example.lol.coredi.CD;
import com.example.lol.coredi.MyCD;
import com.example.lol.coredi.SimpleCondition;
import org.springframework.context.annotation.*;

@Configuration
@EnableAspectJAutoProxy
@Profile("dev")
@PropertySource("classpath:settings.properties")
public class JavaBasedConfig {

    @Bean
    @Conditional(SimpleCondition.class)
    public CD myCD() {
        return new MyCD();
    }

}
