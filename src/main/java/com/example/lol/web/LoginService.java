package com.example.lol.web;

import org.springframework.stereotype.Service;

import com.example.lol.lol.Person;

@Service
public class LoginService {

    public String checkPerson(Person person) {
        if (person.getName().equals("Denis")) {
            return "success";
        }
        return "failed";
    }

}
