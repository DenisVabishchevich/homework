package com.example.lol.web;

import java.io.File;
import java.io.IOException;

import javax.servlet.ServletContext;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;

import com.example.lol.repo.Spitter;

@Controller
@RequestMapping("/spitter")
public class SpitterControler {

    @Autowired
    private ServletContext sc;

    @GetMapping(value = "/register") 
    public String getRegPage(Model model) {
	model.addAttribute("spitter", new Spitter());
	return "reg";
    }

    @PostMapping("/register")
    public String postRegPage(Model model, @RequestPart("profilePicture") MultipartFile mp, @Valid Spitter spitter,
	    BindingResult result) throws IOException {
	mp.transferTo(new File(sc.getRealPath("") + "WEB-INF/upload/" + mp.getOriginalFilename()));
	if (result.hasErrors()) {
	    return "reg";
	}
	return "redirect:/";
    }

}
