package com.example.lol.di;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Main {

    public static void main(String[] args) {
	ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("beans-di.xml");
	Performer performer = (Performer) context.getBean("Adam");
	performer.perform();
	context.close();
    }

}
