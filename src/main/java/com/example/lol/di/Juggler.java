package com.example.lol.di;

public class Juggler implements Performer {

    private int beanBags;

    public Juggler() {
	beanBags = 3;
    }

    public Juggler(int beanBags) {
	this.beanBags = beanBags;
    }

    public void perform() {
	System.out.println("Juggling " + beanBags + " balls");

    }

}
