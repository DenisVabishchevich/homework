package com.example.lol.di;

public interface Performer {

    void perform();
}
