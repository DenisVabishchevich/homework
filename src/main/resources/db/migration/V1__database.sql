create table PERSON (
		PERSON_ID INT NOT null GENERATED ALWAYS AS IDENTITY, 
		NAME varchar(255) not null,
		AGE INT NOT NULL,
        primary key (PERSON_ID)
    ) 