package tests;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

import org.junit.Test;
import org.springframework.test.web.servlet.MockMvc;

import com.example.lol.web.HomeController;
import com.example.lol.web.SpitterControler;

public class HomeControllerTest {

    @Test
    public void testHOmeController() throws Exception {

	HomeController controller = new HomeController();
	MockMvc mock = standaloneSetup(controller).build();
	mock.perform(get("/")).andExpect(view().name("home"));
    }

    @Test
    public void regTest() throws Exception {

	SpitterControler controller = new SpitterControler();
	MockMvc mock = standaloneSetup(controller).build();
	mock.perform(get("/spitter/register")).andExpect(view().name("reg"));
    }

}
