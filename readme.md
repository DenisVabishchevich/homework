﻿WIRING

@Configuration джава конфиг(already @Component can be find by @ComponentScan(baseDir='packageName')
@Bean определение бина в джава конфиге
@ComponentScan поиск анотированих бинов
@Autowired  автоматическое связывание
@Inject     автоматическое связывание
@Named jsr 330 как @Component
@Profile разграничивание профилей для джава конфига ( в web.xml как context-param spring.profiles.default or in property file )
@Import  импортирование конфигураций
@Conditional(MyConditopnal.class) Условие для создания бинов ( расширение интерфейса Condition если true - создается, false - не создается)
@Qualifier("beanName") дает квалификатор бинам и используется для избежания противоречий при внедрении. Better to use yourself Qualifiers
@ActiveProfiles - включить профил для тестов, spring.profiles.active = "dev" property for Spring to lunch active profile
"${}" - for Spring properties, "#{}" for Spring EL
@Value - for inject values like @Value("${property.name}") if PropertySourcesPlaceholderConfigurer is regitred in context.
@PropertySource - load resources to Environment.
Environment - param that holds all properties and some ather stuff. It is a @Component, can be @Autowired. Holds properties from systemProperties and systemEnvironment
@Primary for load primary bean when u have 2 or more same beans (not used)

AOP

Compile time aspects(special compile); Load time aspects(special ClassLoader); Proxy aspects(Generates Proxies)
advice - job;  pointcut -; join points - where to inject aop;
@Aspect - for declaretion an aspect (class annotation) must be registerd in Context or @Component
@Before(execution( )) @AfterReturning (execution()) @AfterThrowing(execution()) @After and @Aroun with method parameter - ProceedJoinpoint 
@Pointcut to declare pointcut to use for many resons
args(param) match with execution(** play(int) $$ args(param)) method sign. like myAspectMeth(int param)
@DeclareParents - for add new funct to the proxy object using aspects

WEB(leetcode.com), FizzBuzz book

Request comes to DispatcherServlet (front controller patern)
-> DS ask HandlerMapping class for specific Controller asotiated with url
-> Controllers process data using Services and push data and view name back to DS
-> DS ask ViewResolver for View assosiated with name pushed back by the Controller
-> DS add data to responseObject and deligete work for View (jsp page or ather)
-> View render data to the user
Since Servlet 3.0 Container LF ServletConteinerInitiolizer(SpringServletConteinerInitiolizer in Spring) and call onStartUp() method, and SpringServletConteinerInitiolizer add all classes which implement WebConteinerInitializer
WebConfig.class load by DS, RootConfig.class load by ContextLoaderListener.class (added automaticaly)
AbstractAnnotationConfigDispatcherServletInitializer.class for init DispatcherServlet(add web.xml) in java class(one class for one DispatcherServlet)
@EnableWebMvc or <mvc:annotation driven/> for add default configuration for Spring web mvc
@RequestMapping for map urls to controllers
There is another way to test controller via MockMvc
@RequestParam for get params from user as ?user=denis&age=27, defaultValue to specify params as strings if they dont present
@PathVarible for get param from path (/spittles/{id} - @PathVarible("id"))
if method att is a class must much with request params (@RequestMapping String classMethod(Clazz c){ save (c) return "string"} )
@Valid for validate classes (jsr-303) if realisation is present (Validation API),(work with Error class exactly after validated parameter) like 'public String met(@Valid Clazz z, Erorr error)'
ViewResolver interface for much log name with view, return type View which render response and model to client()
InternalResourceViewResolver  for jsp (set JstlView for i18n and mesaggin by jstl tags)
@PostMapping for post @GetMapping for get
BindingResult for Validation result (bindingResult.hasError())
Hibernate Validation ref  implimentation for jsr 303 (Validation)
U can specify to use javaBased config in web.xml
MultipartResolver (2 implementation Standart rely on servlet 3.0 and Jacarta Commons FileUpliad API(prefer))
add in <form enctype="multipart/form-data"> and <input type="file" name="profilePicture" accept="image/jpeg,image/png,image/gif"> in html
@RequestPart("fileNmae")  with MultipartFile mpf for get data from client with wider capabilities
@ResponseStatus(value=HttpStatus.NOT_FOUND,reason="Spittle Not Found") for map exeption to error pages (404 not found) if thown in controller
@ExceptionHandler for annotate controller's methods to redirect user errors to specific  user pages @ExceptionHandler(DuplicateSpittleException.class)
@ControllerAdvice-annotated class are applied globally across all @RequestMapping(already @Component)-annotated methods on all controllers in an application. Add all method to all Controllers(aspect)
u can specify redirect path by addin model.addAttrib("name", object) and use return like "redirect:/spitter/{name}", if there is no {name} then add to query param like &name="Denis"
FlashAttributes to store object in session and take it back after redirect (via RedirectAttributes model)

WEB FLOW

WebFlow need to add WebFlowRegistry and WebFlowExecutor beans
AbstractFlowConfiguration with @Configuration for create WebFlow discription class
Flow State Transition Data  4 "flows"
State: start state < action state, dicision state , sub-flow state> end state
FlowExecutor creates an instance of the flow execution for every user who entered to the flow and navigete him through web application(flow)
FlowRegistry - A flow registry’s job is to load flow definitions and make them available to the flow executor
WEB-INF/flows/order/order-flow.xml  "order" is id for this flow ( http://mysite.com/application/order) if u are using location-pattern

SECURITY

url security via servlet filters and method security via spring aop
Registred by impl of AbstractSecurityWebApplicationInitializer (load DelegatingFilterProxy filter)
@Configuration @EnableWeb(Mvc)Security and extends WebSecurityConfigurerAdapter for add Spring security ()
securityFilterChain for web.xml and class = DelegatingFilterProxy.class (register all ather filters)
override 3 methods configure(WebSecurity) configure(HttpSecurity) configure(AuthenticationManagerBuilder)
CSRF added automatecly if using spring forms <sf:form method="POST"> or by hands <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
spring security tags library for jsp views()

DATA

Configuration DataSource: 1 JNDI (jndi.lookUp("java:/comp/env/name"),2 Jdbc Driver, 3 Connection Pool (Apache dbcp, c3po)
JdbcTemplate class to maintain with DB from Spring (better to use JdbcOperations)
flyway tool for database migration(db/migration under src/main/resources)
Lazy loading-allows you to grab data only as it’s needed;
Eager fetching allows you tograb an entire object graph in one query;
Cascading—Sometimes changes to a database table should result in changes to other tables as well.
Need to register LocalSessionFactoryBean (and set DataSource, Entity Classes<LF @Entity or @MappedSuperclass>, DataBase Properties)
No need to HibernateTemplate, u can get Session directy from SessionFactory but need to declare @Bean PersistenceExceptionTranslationPostProcessor for rethrow specific unchaked exception if specific HibernateExceptions has been detected
2 JPA Entety Menegers:  
-Application-managed - application is responsible for opening or closing entity managersand involving the entity manager in transactions
-Container-managed — Entity managers are created and managed by a Java EE container.
@EnableJpaRepositories(basePackages="com.habuma.spittr.db") for Spring Data Repositories under config classes
@Query for define SQL query using in Spring data
@EnableMongoRepositories  for enable Mongo repositoriest via Spring Data
extend AbstractMongoConfiguration for easyly maintenance

CACHING

@EnableCaching for enable spring cashing ( need implementation ) via Spring aspects 

